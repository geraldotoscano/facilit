package br.facilit.controller;

import java.util.List;

import br.com.facilit.dao.ProdutoDAO;
import br.com.facilit.model.Produto;

public class ProdutoController {

	public List<Produto> listarProduto() {
		List<Produto> lista = null;
		try {
			lista = ProdutoDAO.getInstance().listar();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}

	public Produto buscarProduto(int id) {

		try {
			return ProdutoDAO.getInstance().buscar(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String incluirProduto(Produto produto) {

		String retorno;

		try {
			ProdutoDAO.getInstance().inserirAlterar(produto);
			retorno = "Produto incluido com sucesso!";
		} catch (Exception e) {
			retorno = "Ocorreu um erro ao incluir o produto";
			e.printStackTrace();
		}
		return retorno;

	}

	public String alterarProduto(Produto produto) {

		String retorno;

		try {
			ProdutoDAO.getInstance().inserirAlterar(produto);
			retorno = "Produto alterado com sucesso!";
		} catch (Exception e) {
			retorno = "Ocorreu um erro ao alterar o produto";
			e.printStackTrace();
		}
		return retorno;

	}

}
