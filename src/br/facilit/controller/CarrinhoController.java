package br.facilit.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.facilit.dao.CarrinhoDAO;
import br.com.facilit.dao.CupomDAO;
import br.com.facilit.dao.ProdutoDAO;
import br.com.facilit.model.Carrinho;
import br.com.facilit.model.Cupom;
import br.com.facilit.model.Produto;
import br.com.facilit.model.ProdutoCarrinho;

public class CarrinhoController {

	public List<Carrinho> listarCarrinho() {
		List<Carrinho> lista = null;
		try {
			lista = CarrinhoDAO.getInstance().listar();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}

	public Carrinho buscarCarrinho(int id) {

		try {
			Carrinho carrinho = CarrinhoDAO.getInstance().buscar(id);
			
			if(carrinho.getCupom()!=null) {
				carrinho.setValorCupom(carrinho.getCupom().getValor());
			}
			double valorTotal=0;
			for(ProdutoCarrinho atual: carrinho.getProdutoCarrinho()) {
				if(atual.getQtd()>=10) {
					double descontoAtual = carrinho.getValorDescontoQTD();
					
					double desconto = (atual.getProduto().getValor()*10)/100;
					
					desconto =desconto*atual.getQtd();
					
					carrinho.setValorDescontoQTD(desconto+descontoAtual);
				}
				valorTotal+=atual.getProduto().getValor()*atual.getQtd();
			}
			carrinho.setValorTotal(valorTotal);
			if (valorTotal-carrinho.getValorCupom()-carrinho.getValorDescontoQTD()>=10000) {
				carrinho.setValorDescontoValor((valorTotal-carrinho.getValorCupom()-carrinho.getValorDescontoQTD())*10/100);
			}else if(valorTotal-carrinho.getValorCupom()-carrinho.getValorDescontoQTD()>=5000) {
				carrinho.setValorDescontoValor((valorTotal-carrinho.getValorCupom()-carrinho.getValorDescontoQTD())*7/100);
			}else if(valorTotal-carrinho.getValorCupom()-carrinho.getValorDescontoQTD()>=1000) {
				carrinho.setValorDescontoValor((valorTotal-carrinho.getValorCupom()-carrinho.getValorDescontoQTD())*5/100);
			}
			
			carrinho.setValorReal(carrinho.getValorTotal()-carrinho.getValorCupom()-carrinho.getValorDescontoQTD()-carrinho.getValorDescontoValor());
			
			
			return carrinho;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String adicionarItemCarrinho(int idCarrinho, int idProduto) {
		String retorno;
		try {
			Produto produto = ProdutoDAO.getInstance().buscar(idProduto);
			Carrinho carrinho = CarrinhoDAO.getInstance().buscar(idCarrinho);
			
			if(carrinho.getProdutoCarrinho()==null) {
				carrinho.setProdutoCarrinho(new ArrayList<ProdutoCarrinho>());
			}
			boolean possui=false;
			for(int i=0;i<carrinho.getProdutoCarrinho().size();i++ ) {
				ProdutoCarrinho atual =carrinho.getProdutoCarrinho().get(i); 
				if(atual.getProduto().getId()==produto.getId()) {
					possui=true;
					atual.setQtd(atual.getQtd()+1);
					carrinho.getProdutoCarrinho().set(i, atual);
				}
			}
			
			if(!possui) {
				ProdutoCarrinho pc = new ProdutoCarrinho();
				pc.setCarrinho(carrinho);
				pc.setProduto(produto);
				pc.setQtd(1);
				carrinho.getProdutoCarrinho().add(pc);
			}
			
			CarrinhoDAO.getInstance().inserirAlterar(carrinho);
			retorno = "Item Inserido com sucesso!";
		} catch (Exception e) {
			e.printStackTrace();
			retorno = "Ocorreu um erro ao inserir o item.";
		}
		return retorno;
	}

	public String atualizarCupomCarrinho(int idCarrinho, int idCupom) {
		String retorno;
		try {
			Cupom cupom = CupomDAO.getInstance().buscar(idCupom);
			if (cupom==null) {
				return "Cupom invalido!";
			}else if(cupom.getValidade().after(new Date())) {
				return "Cupom vencido";
			}
				
			Carrinho carrinho = CarrinhoDAO.getInstance().buscar(idCarrinho);
			carrinho.setCupom(cupom);
			CarrinhoDAO.getInstance().inserirAlterar(carrinho);
			retorno = "Cupom Atualizado com sucesso!";
		} catch (Exception e) {
			e.printStackTrace();
			retorno = "Ocorreu um erro ao atualizar o cupom.";
		}
		return retorno;
	}

	public String removerCupomCarrinho(int idCarrinho) {
		String retorno;
		try {
			Carrinho carrinho = CarrinhoDAO.getInstance().buscar(idCarrinho);
			carrinho.setCupom(null);
			CarrinhoDAO.getInstance().inserirAlterar(carrinho);
			retorno = "Cupom Removido com sucesso!";
		} catch (Exception e) {
			e.printStackTrace();
			retorno = "Ocorreu um erro ao remover o cupom.";
		}
		return retorno;
	}

	public String removerItemCarrinho(int idCarrinho, int idProduto) {
		String retorno;
		try {
			Produto produto = ProdutoDAO.getInstance().buscar(idProduto);
			
			
			Carrinho carrinho =CarrinhoDAO.getInstance().buscar(idCarrinho);

				if (carrinho.getProdutoCarrinho() == null) {
					carrinho.setProdutoCarrinho(new ArrayList<ProdutoCarrinho>());
				}

				List<ProdutoCarrinho> lista = carrinho.getProdutoCarrinho();
				boolean achou=false;				
				for (int i = 0; i < lista.size() && !achou; i++) {
					ProdutoCarrinho atual = (ProdutoCarrinho) lista.get(i);
					if (atual.getProduto().getId() == produto.getId()) {
						atual.setQtd(atual.getQtd()-1);
						
						if(atual.getQtd()==0) {
							lista.remove(i);
						}else{
							lista.set(i, atual);
						}
						achou=true;
					}
				}
				carrinho.setProdutoCarrinho(lista);

				CarrinhoDAO.getInstance().inserirAlterar(carrinho);
			
			retorno = "Item removido com sucesso!";
		} catch (Exception e) {
			e.printStackTrace();
			retorno = "Ocorreu um erro ao remover o item.";
		}
		return retorno;
	}

	public String incluirCarrinho(Carrinho carrinho) {

		String retorno;

		try {
			CarrinhoDAO.getInstance().inserirAlterar(carrinho);
			retorno = "Carrinho incluido com sucesso!";
		} catch (Exception e) {
			retorno = "Ocorreu um erro ao incluir o Carrinho";
			e.printStackTrace();
		}
		return retorno;

	}

	public String alterarCarrinho(Carrinho carrinho) {

		String retorno;

		try {
			CarrinhoDAO.getInstance().inserirAlterar(carrinho);
			retorno = "Carrinho alterado com sucesso!";
		} catch (Exception e) {
			retorno = "Ocorreu um erro ao alterar o carrinho";
			e.printStackTrace();
		}
		return retorno;

	}

}
