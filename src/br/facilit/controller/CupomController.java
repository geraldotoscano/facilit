package br.facilit.controller;

import java.util.List;

import br.com.facilit.dao.CupomDAO;
import br.com.facilit.model.Cupom;

public class CupomController {

	public List<Cupom> listarCupom() {
		List<Cupom> lista = null;
		try {
			lista = CupomDAO.getInstance().listar();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}

	public Cupom buscarCupom(int id) {

		try {
			return CupomDAO.getInstance().buscar(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String incluirCupom(Cupom cupom) {

		String retorno;

		try {
			CupomDAO.getInstance().inserirAlterar(cupom);
			retorno = "Cupom incluido com sucesso!";
		} catch (Exception e) {
			retorno = "Ocorreu um erro ao incluir o Cupom";
			e.printStackTrace();
		}
		return retorno;

	}

	public String alterarCupom(Cupom cupom) {

		String retorno;

		try {
			CupomDAO.getInstance().inserirAlterar(cupom);
			retorno = "Cupom alterado com sucesso!";
		} catch (Exception e) {
			retorno = "Ocorreu um erro ao alterar o cupom";
			e.printStackTrace();
		}
		return retorno;

	}

}
