package br.com.facilit.resource;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.facilit.model.Carrinho;
import br.facilit.controller.CarrinhoController;



@Path("/carrinho")
public class CarrinhoResource {

	private CarrinhoController carrinhoController;

	@PostConstruct
	private void init() {
		carrinhoController = new CarrinhoController();
	}
	
	@GET
	@Path("/listar")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Carrinho> listarCarrinho() {

		return carrinhoController.listarCarrinho();

	}

	@GET
	@Path("/buscar/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Carrinho buscarCarrinho(@PathParam("id") int id) {

		return carrinhoController.buscarCarrinho(id);

	}

	@GET
	@Path("/adicionarItem/{idCarrinho}/{idProduto}")
	@Produces(MediaType.APPLICATION_JSON)
	public String adicionarItemCarrinho(@PathParam("idCarrinho") int idCarrinho,
			@PathParam("idProduto") int idProduto) {
		return carrinhoController.adicionarItemCarrinho(idCarrinho, idProduto);
	}

	@GET
	@Path("/atualizarCupom/{idCarrinho}/{idCupom}")
	@Produces(MediaType.APPLICATION_JSON)
	public String atualizarCupomCarrinho(@PathParam("idCarrinho") int idCarrinho, @PathParam("idCupom") int idCupom) {
		return carrinhoController.atualizarCupomCarrinho(idCarrinho, idCupom);
	}

	@GET
	@Path("/removerCupom/{idCarrinho}")
	@Produces(MediaType.APPLICATION_JSON)
	public String removerCupomCarrinho(@PathParam("idCarrinho") int idCarrinho) {
		return carrinhoController.removerCupomCarrinho(idCarrinho);
	}

	@GET
	@Path("/removerItem/{idCarrinho}/{idProduto}")
	@Produces(MediaType.APPLICATION_JSON)
	public String removerItemCarrinho(@PathParam("idCarrinho") int idCarrinho, @PathParam("idProduto") int idProduto) {
		return carrinhoController.removerItemCarrinho(idCarrinho, idProduto);
	}

	@POST
	@Path("/incluir")
	@Consumes(MediaType.APPLICATION_JSON)
	public String incluirCarrinho(Carrinho carrinho) {

		return carrinhoController.incluirCarrinho(carrinho);

	}

	@POST
	@Path("/alterar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String alterarCarrinho(Carrinho carrinho) {

		return carrinhoController.alterarCarrinho(carrinho);

	}

}
