package br.com.facilit.resource;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.facilit.model.Cupom;
import br.facilit.controller.CupomController;


@Path("/cupom")
public class CupomResource {

	private CupomController cupomController;

	@PostConstruct
	private void init() {
		cupomController = new CupomController();
	}
	
	@GET
	@Path("/listar")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Cupom> listarCupom() {

		return cupomController.listarCupom();
	}

	@GET
	@Path("/buscar/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Cupom buscarCupom(@PathParam("id") int id) {

		return cupomController.buscarCupom(id);

	}

	@POST
	@Path("/incluir")
	@Consumes(MediaType.APPLICATION_JSON)
	public String incluirCupom(Cupom cupom) {

		return cupomController.incluirCupom(cupom);

	}

	@POST
	@Path("/alterar/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String alterarCupom(@PathParam("id") int id, Cupom cupom) {

		cupom.setId(id);
		return cupomController.alterarCupom(cupom);

	}

}
