package br.com.facilit.resource;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.facilit.model.Produto;
import br.facilit.controller.ProdutoController;

@Path("/produto")
public class ProdutoResource {

	private ProdutoController produtoController;

	@PostConstruct
	private void init() {
		produtoController = new ProdutoController();
	}

	@GET
	@Path("/listar")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Produto> listarProduto() {
		List<Produto> lista = null;

		lista = produtoController.listarProduto();
	//	lista.get(0).setProdutoCarrinho(null);
		return lista;
	}

	@GET
	@Path("/buscar/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Produto buscarProduto(@PathParam("id") int id) {

		return produtoController.buscarProduto(id);

	}

	@POST
	@Path("/incluir")
	@Consumes(MediaType.APPLICATION_JSON)
	public String incluirProduto(Produto produto) {

		return produtoController.incluirProduto(produto);

	}

	@POST
	@Path("/alterar/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String alterarProduto(@PathParam("id") int id, Produto produto) {

		produto.setId(id);
		return produtoController.alterarProduto(produto);

	}

}
