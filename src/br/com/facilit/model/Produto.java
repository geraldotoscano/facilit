package br.com.facilit.model;


import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "produto")
public class Produto implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "descricao")
	private String descricao;
	@Column(name = "valor")
	private double valor;
	@OneToMany(mappedBy = "produto",fetch = FetchType.LAZY)
	private Set<ProdutoCarrinho> produtoCarrinho;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Set<ProdutoCarrinho> getProdutoCarrinho() {
		return produtoCarrinho;
	}

	public void setProdutoCarrinho(Set<ProdutoCarrinho> produtoCarrinho) {
		this.produtoCarrinho = produtoCarrinho;
	}
	
	

}
