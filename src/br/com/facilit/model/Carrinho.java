package br.com.facilit.model;

import java.io.Serializable;
import java.util.List;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "carrinho")
public class Carrinho  implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(columnDefinition = "cliente_id")
	private Cliente cliente;

	@OneToMany(mappedBy = "carrinho",fetch = FetchType.LAZY)
	private List<ProdutoCarrinho> produtoCarrinho;

	@OneToOne
	@JoinColumn(columnDefinition = "cupom_id")
	private Cupom cupom;

	@Transient
	private double valorTotal;
	@Transient
	private double valorDescontoQTD;
	@Transient
	private double valorDescontoValor;
	@Transient
	private double valorCupom;
	@Transient
	private double valorReal;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Cupom getCupom() {
		return cupom;
	}

	public void setCupom(Cupom cupom) {
		this.cupom = cupom;
	}

	public double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public double getValorDescontoQTD() {
		return valorDescontoQTD;
	}

	public void setValorDescontoQTD(double valorDescontoQTD) {
		this.valorDescontoQTD = valorDescontoQTD;
	}

	public double getValorDescontoValor() {
		return valorDescontoValor;
	}

	public void setValorDescontoValor(double valorDescontoValor) {
		this.valorDescontoValor = valorDescontoValor;
	}

	public double getValorCupom() {
		return valorCupom;
	}

	public void setValorCupom(double valorCupom) {
		this.valorCupom = valorCupom;
	}

	public double getValorReal() {
		return valorReal;
	}

	public void setValorReal(double valorReal) {
		this.valorReal = valorReal;
	}

	public List<ProdutoCarrinho> getProdutoCarrinho() {
		return produtoCarrinho;
	}

	public void setProdutoCarrinho(List<ProdutoCarrinho> produtoCarrinho) {
		this.produtoCarrinho = produtoCarrinho;
	}



}
