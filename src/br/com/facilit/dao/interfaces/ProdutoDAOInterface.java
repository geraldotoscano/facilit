package br.com.facilit.dao.interfaces;

import java.util.List;

import br.com.facilit.model.Produto;

public interface ProdutoDAOInterface {

	List<Produto> listar();

	void inserirAlterar(Produto produto);

	Produto buscar(int id);

}