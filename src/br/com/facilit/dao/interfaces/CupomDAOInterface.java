package br.com.facilit.dao.interfaces;

import java.util.List;

import br.com.facilit.model.Cupom;

public interface CupomDAOInterface {

	List<Cupom> listar();

	void inserirAlterar(Cupom cupom);

	Cupom buscar(int id);

}