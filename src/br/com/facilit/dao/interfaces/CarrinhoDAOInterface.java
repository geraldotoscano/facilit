package br.com.facilit.dao.interfaces;

import java.util.List;

import br.com.facilit.model.Carrinho;


public interface CarrinhoDAOInterface {


	List<Carrinho> listar();

	void inserirAlterar(Carrinho carrinho);

	Carrinho buscar(int id);

}