package br.com.facilit.dao;

import java.util.List;

import org.hibernate.query.Query;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import br.com.facilit.dao.interfaces.CupomDAOInterface;
import br.com.facilit.model.Cupom;
import br.com.facilit.util.HibernateUtil;

public class CupomDAO implements CupomDAOInterface {

	private static CupomDAOInterface instance;

	public static CupomDAOInterface getInstance() {
		if (instance == null)
			instance = new CupomDAO();
		return instance;
	}

	@Override
	public List<Cupom> listar() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {

			session.beginTransaction();
			return session.createQuery("FROM Cupom").list();
		} catch (HibernateException e) {
			if (session.getTransaction() != null)
				session.getTransaction().rollback();
		} finally {
			session.close();
		}

		return null;
	}

	@Override
	public void inserirAlterar(Cupom cupom) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		try {

			session.beginTransaction();

			session.saveOrUpdate(cupom);
			session.getTransaction().commit();

		} catch (HibernateException e) {
			if (session.getTransaction() != null)
				session.getTransaction().rollback();
		} finally {
			session.close();
		}

	}

	@Override
	public Cupom buscar(int id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {

			session.beginTransaction();
			Query query = session.createQuery("FROM Cupom where id=:id");
			query.setParameter("id", id);

			List<Cupom> lista = query.list();
			if (!lista.isEmpty()) {
				return lista.get(0);
			}
		} catch (HibernateException e) {
			if (session.getTransaction() != null)
				session.getTransaction().rollback();
		} finally {
			session.close();
		}

		return null;
	}

}
