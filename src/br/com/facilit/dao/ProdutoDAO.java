package br.com.facilit.dao;

import java.util.List;

import org.hibernate.query.Query;


import org.hibernate.HibernateException;
import org.hibernate.Session;

import br.com.facilit.dao.interfaces.ProdutoDAOInterface;
import br.com.facilit.model.Produto;
import br.com.facilit.util.HibernateUtil;

public class ProdutoDAO implements ProdutoDAOInterface {

	private static ProdutoDAOInterface instance;

	public static ProdutoDAOInterface getInstance() {
		if (instance == null)
			instance = new ProdutoDAO();
		return instance;
	}

	@Override
	public List<Produto> listar() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {

			session.beginTransaction();
			return session.createQuery("FROM Produto").list();
		} catch (HibernateException e) {
			if (session.getTransaction() != null)
				session.getTransaction().rollback();
		} finally {
			session.close();
		}

		return null;
	}

	@Override
	public void inserirAlterar(Produto produto) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		try {

			session.beginTransaction();

			session.saveOrUpdate(produto);
			session.getTransaction().commit();

		} catch (HibernateException e) {
			if (session.getTransaction() != null)
				session.getTransaction().rollback();
		} finally {
			session.close();
		}

	}

	@Override
	public Produto buscar(int id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {

			session.beginTransaction();
			Query query = session.createQuery("FROM Produto where id=:id");
			query.setParameter("id", id);

			List<Produto> lista = query.list();
			if (!lista.isEmpty()) {
				return lista.get(0);
			}
		} catch (HibernateException e) {
			if (session.getTransaction() != null)
				session.getTransaction().rollback();
		} finally {
			session.close();
		}

		return null;
	}

}
