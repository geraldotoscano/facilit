package br.com.facilit.dao;


import java.util.List;


import org.hibernate.query.Query;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import br.com.facilit.dao.interfaces.CarrinhoDAOInterface;
import br.com.facilit.model.Carrinho;
import br.com.facilit.util.HibernateUtil;

public class CarrinhoDAO implements CarrinhoDAOInterface {

	private static CarrinhoDAOInterface instance;

	public static CarrinhoDAOInterface getInstance() {
		if (instance == null)
			instance = new CarrinhoDAO();
		return instance;
	}




	@Override
	public List<Carrinho> listar() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {

			session.beginTransaction();
			return session.createQuery("FROM Carrinho").list();
		} catch (HibernateException e) {
			if (session.getTransaction() != null)
				session.getTransaction().rollback();
		} finally {
			session.close();
		}

		return null;
	}

	@Override
	public void inserirAlterar(Carrinho carrinho) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		try {

			session.beginTransaction();

			session.saveOrUpdate(carrinho);
			session.getTransaction().commit();

		} catch (HibernateException e) {
			if (session.getTransaction() != null)
				session.getTransaction().rollback();
		} finally {
			session.close();
		}

	}

	@Override
	public Carrinho buscar(int id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {

			session.beginTransaction();
			Query query = session.createQuery("FROM Carrinho where id=:id");
			query.setParameter("id", id);

			List<Carrinho> lista = query.list();
			if (!lista.isEmpty()) {
				return lista.get(0);
			}
		} catch (HibernateException e) {
			if (session.getTransaction() != null)
				session.getTransaction().rollback();
		} finally {
			session.close();
		}

		return null;
	}

}
